const express = require(`express`);
const mongoose = require(`mongoose`);

const app = express();
const port = 3000;


app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://melaidotollo:Ezekiel04@cluster0.wixhc.mongodb.net/b125-tasks?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

	).then(() => {
		console.log("Success");
	}).catch((error) => {
		console.log(error);
	});

//----------------------------------------------------------------

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: Boolean,
		default: false
	}

});

const Task = mongoose.model('Task', taskSchema);

//-------------------------------------------------------------------------

const userSchema = new mongoose.Schema({

	firstName: String,
	lastName: String,
	userName: String,
	password: String
});

const User = mongoose.model('User', userSchema);

//----------------------------------------
// insert new task\aa

app.post('/add-task', (req,res) => {

	let newTask = new Task({
		name: req.body.name
	});

	newTask.save((err,saveTask) => {
		if (err) {
			console.log(err);

		} else {
			res.send(`New task saved! ${saveTask}`);
		}
	});


});

//----------------------------------------
// insert new task\aa

app.post('/register', (req,res) => {

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		userName: req.body.userName, 
		password: req.body.password
	});

	newUser.save((err,saveUser) => {
		if (err) {
			console.log(err);

		} else {
			res.send(`New user saved! ${saveUser}`);
		}
	});


});

app.get('/retrieve-tasks',(req,res) => {

	Task.find({}, (error,records)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(records)
		}


	});

});


app.get('/retrieve-tasks-done',(req,res) => {

	Task.find({ status: true }, (error,records)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(records);
		}


	});

});


app.put('/complete-task/:taskId',(req,res) => {

	let taskId = req.params.taskId;
	Task.findByIdAndUpdate(taskId, { status: true }, (error,updatedTasks)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(`Task Complete`);
		}


	});

});


app.delete('/delete-task/:taskId',(req,res) => {

	let taskId = req.params.taskId;
	Task.findByIdAndDelete(taskId, (error,deletedTasks)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(`Task deleted!`);
		}


	});

});
/*[{
    "firstName": "Peter",
    "lastName": "Parker",
    "userName": "spidey", 
    "password": "spiderman2021"
},
{
    "firstName": "Gwen",
    "lastName": "Stacy",
    "userName": "gwen_s", 
    "password": "gwenstacy2021"
},
{
    "firstName": "Doctor",
    "lastName": "Strange",
    "userName": "dr_strange", 
    "password": "drstrangesince2021"
}]

*/


app.listen(port,()=> console.log(`Server is running at ${port}`));